<?php

// Example settings.php file for all environments.

// Include local settings file for development.
$local_settings = getcwd() . '/sites/default/settings.local.php';
if (file_exists($local_settings)) {
  include $local_settings;
}
