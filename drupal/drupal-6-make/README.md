# BOI CI Drupal 6 Drush Make Example

This folder contains an example of how you could set up a Drupal 6 site
that's built with drush make and pushed to Pantheon when commits hit origin/master.

## BOI CI Specific Configuration

### Build

In the `.boi_ci.yml` file, there is a section called "build".

#### Build Root

`build.root` allows you to define where your project will be built into, both for local development and when pushing to external environments. In this example, the build root is "www". When running `composer install` all of the build assets will be placed into the www sub-directory. The build root should be excluded in your .gitignore file.

### Temporary Directory

A writable temp directory is required to have in your .boi-ci.yml file.
Usually `/tmp` will suffice.

### Symlinks

In the `.boi_ci.yml` file, there is a section called "symlinks". This section allows you to define files or folders outside of your build root that should be included in your build.

In this example, we're sym-linking or custom modules, themes, and libraries folders from the root of the project into www/sites/default/______ (note the "www" is inferred from your build root). This is also the location they will be when pushing the code to your external environments.

We use symlinks so that you can edit the files at the root of your project during development and have them update in your active build directory. In otherwords, you don't have to run `composer install` every time you make a change to your custom module during development.

The required values for a symlink are are "source" and "destination". Source is the source relative to your project root. Destination is the destination relative to your build root. For example, the ./local/settings.local.php file in the example config will be placed into www/sites/default/settings.local.php (note "www" is inferred from your build root).

We typically use a `./local` directory during local development that contains settings.php and files (sites/default/files). To follow this method, create `./local/settings.local.php` (file) and `./local/files` (directory). Then either re-run `composer install` or run `bin/boi_ci build:symlinks`

### Environments

The environments section allows you to define external environments that will be used to push git artifacts to. An artifact is another git repo that contains all of your build files. We use artifacts for several reasons, but the main reason is that most hosting providers provide their own git repo for pushing to their environment. BOI CI allows you to build your project into their repos and provides the ability to have a common workflow, regardless of the hosting provider. You can even push your artifacts to multiple hosting providers from the same project, as seen in the example.

#### Git Artifact Repos

Each environment should have a git artifact repo defined. This represents the git remote and branch that will be pushed to whenever a build it being deployed.
