module.exports = function(grunt) {
    require('load-grunt-tasks')(grunt); // npm install --save-dev load-grunt-tasks


    // use the base theme's sass dir as an include path
    // themes should @import 'basetheme/core' in their style.scss
    var sass_include_paths = [
        './sass'
    ];

    // each theme's main sass endpoint file
    var sass_files = {
        'css/main.css': 'sass/main.scss'
    };

    // files that trigger sass compilation when grunt watch-ing
    var watch_files = [
        'sass/**/*.{scss,sass}'
    ];

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        // https://github.com/sass/node-sass
        sass: {
            dist: {
                options: {
                    outputStyle: 'compressed',
                    sourceMap: false,
                    includePaths: sass_include_paths,
                },
                files: sass_files
            },

            dev: {
                options: {
                    outputStyle: 'expanded',
                    sourceMap: true,
                    includePaths: sass_include_paths,
                },
                files: sass_files
            }
        },

        watch: {
            sass: {
                files: watch_files,
                tasks: ['sass:dev' ]
            }
        }

    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // register task
    // default to dev
    grunt.registerTask('default', ['watch'] );
    // grunt.registerTask('dist', ['sass:dist',  'autoprefixer']);
};