# BOI CI Examples!

This repo contains examples of how you can build any Drupal project by utilizing [BOI CI (https://gitlab.com/blueoakinteractive/boi_ci)](https://gitlab.com/blueoakinteractive/boi_ci) and running

`composer install`

This allows you to establish a common development workflow for all of your projects regardless of the Drupal version or hosting provider.
